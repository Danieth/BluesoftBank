package com.daniel.bluesoftbank.bank.utils;

import java.util.Date;
import java.util.List;

public class ParseDatePath {
    public Date parseDate(String datePath) {
        List<String> dateArray = List.of(datePath.split("-"));
        Date date = new Date();
        date.setYear(Integer.parseInt(dateArray.get(0)));
        date.setMonth(Integer.parseInt(dateArray.get(1)));
        return date;
    }
}
