package com.daniel.bluesoftbank.bank.utils;

public interface IMapper <I, O>{
    public O map(I in);
}
