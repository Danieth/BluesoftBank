package com.daniel.bluesoftbank.bank.api.moneyTransfer.service.dto.external;

import lombok.Data;

@Data
public class ConsignMoneyInDTO {
    private final String bankAccountNumber;
    private final Double amount;
}
