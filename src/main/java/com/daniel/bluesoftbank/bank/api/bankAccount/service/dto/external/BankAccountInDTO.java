package com.daniel.bluesoftbank.bank.api.bankAccount.service.dto.external;

import lombok.Data;

@Data
public class BankAccountInDTO {
    private String city;
    private String clientDni;
    private Double balance;
}
