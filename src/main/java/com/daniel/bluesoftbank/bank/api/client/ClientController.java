package com.daniel.bluesoftbank.bank.api.client;

import com.daniel.bluesoftbank.bank.api.client.service.ClientServices;
import com.daniel.bluesoftbank.bank.api.client.service.dto.external.ClientInDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/clients")
public class ClientController {
    private final ClientServices service;

    public ClientController(ClientServices service) {
        this.service = service;
    }

    @PostMapping()
    public void create(@RequestBody ClientInDTO dto) {
        this.service.create(dto);
    }
}
