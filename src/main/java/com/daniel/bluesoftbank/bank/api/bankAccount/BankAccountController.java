package com.daniel.bluesoftbank.bank.api.bankAccount;

//import com.daniel.bluesoftbank.bank.api.bankAccount.model.BankAccount;
import com.daniel.bluesoftbank.bank.api.bankAccount.service.BankAccountService;
import com.daniel.bluesoftbank.bank.api.bankAccount.service.dto.external.BankAccountInDTO;
import com.daniel.bluesoftbank.bank.api.bankAccount.service.dto.internal.BankAccountOutDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/bank-accounts")
public class BankAccountController {
    private final BankAccountService service;

    public BankAccountController(BankAccountService service) {
        this.service = service;
    }

    @PostMapping
    public void create(@RequestBody BankAccountInDTO dto) {
        this.service.create(dto);
    }
    @GetMapping("{number}")
    public ResponseEntity<BankAccountOutDTO> findByNumber(@PathVariable("number") String number) {
        return ResponseEntity.ok(this.service.findByNumber(number));
    }
}
