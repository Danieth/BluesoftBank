package com.daniel.bluesoftbank.bank.api.bankAccount.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Setter
@Getter
public class BankAccountRequestException extends RuntimeException  {
    private HttpStatus httpStatus;
    public BankAccountRequestException(String s, HttpStatus httpStatus) {
        super(s);
        this.httpStatus = httpStatus;
    }

}
