package com.daniel.bluesoftbank.bank.api.bankAccount.service;

import com.daniel.bluesoftbank.bank.api.bankAccount.exception.BankAccountRequestException;
import com.daniel.bluesoftbank.bank.api.bankAccount.mapper.BankAccountInDtoToBankAccount;
import com.daniel.bluesoftbank.bank.api.bankAccount.mapper.BankAccountOutDtoToBankAccount;
import com.daniel.bluesoftbank.bank.api.bankAccount.model.BankAccount;
import com.daniel.bluesoftbank.bank.api.bankAccount.repository.IBankAccountRepository;
import com.daniel.bluesoftbank.bank.api.bankAccount.service.dto.external.BankAccountInDTO;
import com.daniel.bluesoftbank.bank.api.bankAccount.service.dto.internal.BankAccountOutDTO;
import com.daniel.bluesoftbank.bank.api.client.repository.IClientRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Service
public class BankAccountService {
    private final IBankAccountRepository repository;
    private final IClientRepository clientRepository;
    private final BankAccountInDtoToBankAccount mapperIn;
    private final BankAccountOutDtoToBankAccount matterOut;


    public BankAccountService(IBankAccountRepository repository, IClientRepository clientRepository, BankAccountInDtoToBankAccount bankAccountInDtoToBankAccount, BankAccountOutDtoToBankAccount matterOut) {
        this.repository = repository;
        this.clientRepository = clientRepository;
        this.mapperIn = bankAccountInDtoToBankAccount;
        this.matterOut = matterOut;
    }

    public void create(BankAccountInDTO dto) {
        BankAccount bankAccount = this.mapperIn.map(dto);
        bankAccount.setClientDni(this.clientRepository.findById(dto.getClientDni()).get());
        this.repository.save(bankAccount);
    }

    public BankAccountOutDTO findByNumber(String number) {
        Optional<BankAccount> bankAccount = this.repository.findById(Long.parseLong(number));
        if (bankAccount.isPresent()) return this.matterOut.map(bankAccount.get());
        throw new BankAccountRequestException("The bank account does not exist", BAD_REQUEST);
    }
    @Transactional
    public Double addBalance(String number, Double amount) {
        Optional<BankAccount> bankAccount = this.repository.findById(Long.parseLong(number));
        if (bankAccount.isPresent()) {
            this.repository.addAmount(Long.parseLong(number), amount);
            return bankAccount.get().getBalance() + amount;
        }
        throw new BankAccountRequestException("The bank account does not exist", BAD_REQUEST);
    }
}
