package com.daniel.bluesoftbank.bank.api.moneyTransfer.mapper;

import com.daniel.bluesoftbank.bank.api.moneyTransfer.model.MoneyTransfer;
import com.daniel.bluesoftbank.bank.api.moneyTransfer.service.dto.internal.ConsignMoneyOutDTO;
import com.daniel.bluesoftbank.bank.utils.IMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MoneyTransferToConsignMoney implements IMapper<MoneyTransfer, ConsignMoneyOutDTO> {
    @Autowired
    private final ModelMapper modelMapper;

    public MoneyTransferToConsignMoney(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public ConsignMoneyOutDTO map(MoneyTransfer in) {
        return this.modelMapper.map(in, ConsignMoneyOutDTO.class);
    }
}
