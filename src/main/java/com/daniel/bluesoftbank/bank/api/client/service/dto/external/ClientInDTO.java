package com.daniel.bluesoftbank.bank.api.client.service.dto.external;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ClientInDTO {
    @JsonProperty(value = "dni")
    private String dni;
    @JsonProperty(value = "firstName")
    private String firstName;
    @JsonProperty(value = "lastName")
    private String lastName;
}
