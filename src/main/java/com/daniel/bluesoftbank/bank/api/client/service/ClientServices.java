package com.daniel.bluesoftbank.bank.api.client.service;

import com.daniel.bluesoftbank.bank.api.client.mapper.ClientInDtoToClient;
import com.daniel.bluesoftbank.bank.api.client.repository.IClientRepository;
import com.daniel.bluesoftbank.bank.api.client.service.dto.external.ClientInDTO;
import org.springframework.stereotype.Service;

@Service
public class ClientServices {
    private final IClientRepository repository;
    private final ClientInDtoToClient clientInDtoToClient;

    public ClientServices(IClientRepository repository, ClientInDtoToClient clientInDtoToClient) {
        this.repository = repository;
        this.clientInDtoToClient = clientInDtoToClient;
    }

    public void create(ClientInDTO dto) {
        this.repository.save(this.clientInDtoToClient.map(dto));
//        this.repository.save(dto);
    }
}
