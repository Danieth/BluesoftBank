package com.daniel.bluesoftbank.bank.api.bankAccount.model;

public enum BankAccountType { SAVINGS, CURRENT}
