package com.daniel.bluesoftbank.bank.api.bankAccount.service.dto.internal;

import lombok.Data;

import java.time.LocalDate;

@Data
public class BankAccountOutDTO {
    private String number;
    private String city;
    private Double balance;
    private LocalDate createdAt;
}
