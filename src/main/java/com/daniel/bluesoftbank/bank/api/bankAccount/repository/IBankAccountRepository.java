package com.daniel.bluesoftbank.bank.api.bankAccount.repository;

import com.daniel.bluesoftbank.bank.api.bankAccount.model.BankAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IBankAccountRepository extends JpaRepository<BankAccount, Long> {
    @Modifying
    @Query(
            value = "UPDATE BANK_ACCOUNT SET balance = (SELECT balance + :amount WHERE id=:id) WHERE id = :id",
            nativeQuery = true)
    void addAmount(@Param("id") Long id, @Param("amount") Double amount);
}
