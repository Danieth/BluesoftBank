package com.daniel.bluesoftbank.bank.api.client.service.dto.internal;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ClientOutDTO {
    @JsonProperty(value = "dni")
    private String dni;
    @JsonProperty(value = "firstName")
    private String firstName;
    @JsonProperty(value = "lastName")
    private String lastName;
}
