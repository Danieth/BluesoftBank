package com.daniel.bluesoftbank.bank.api.client.repository;

import com.daniel.bluesoftbank.bank.api.client.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IClientRepository extends JpaRepository<Client, String> {
}
