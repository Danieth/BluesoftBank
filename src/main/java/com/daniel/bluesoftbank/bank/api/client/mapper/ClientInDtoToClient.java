package com.daniel.bluesoftbank.bank.api.client.mapper;

import com.daniel.bluesoftbank.bank.api.client.model.Client;
import com.daniel.bluesoftbank.bank.utils.IMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.daniel.bluesoftbank.bank.api.client.service.dto.external.ClientInDTO;

@Component
public class ClientInDtoToClient implements IMapper<ClientInDTO, Client> {
    @Autowired
    private  final ModelMapper modelMapper;

    public ClientInDtoToClient(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public Client map(ClientInDTO in) {
        return this.modelMapper.map(in, Client.class);
    }
}
