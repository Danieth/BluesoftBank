package com.daniel.bluesoftbank.bank.api.moneyTransfer.repository;

import com.daniel.bluesoftbank.bank.api.moneyTransfer.model.MoneyTransfer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Repository
public interface IMoneyTransferRepository extends JpaRepository<MoneyTransfer, UUID> {
    @Query(value = "SELECT * FROM MONEY_TRANSFER mt WHERE mt.BANK_ACCOUNT_RECEIVE = ?1 OR mt.BANK_ACCOUNT_SEND = ?1 LIMIT ?3 OFFSET ?2 * ?3",
            nativeQuery = true)
    List<MoneyTransfer> hitorical(String number, int page, int size);

    @Query(value = "SELECT * FROM (SELECT * FROM MONEY_TRANSFER WHERE BANK_ACCOUNT_RECEIVE = :number OR BANK_ACCOUNT_SEND = :number) WHERE CREATED_AT > :start AND CREATED_AT < :end", nativeQuery = true)
    List<MoneyTransfer> monthlyStatement(@Param("number") String number, @Param("start") Date start,@Param("end") Date end);
}
