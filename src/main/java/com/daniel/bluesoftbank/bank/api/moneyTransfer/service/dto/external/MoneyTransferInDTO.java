package com.daniel.bluesoftbank.bank.api.moneyTransfer.service.dto.external;

import lombok.Data;

@Data
public class MoneyTransferInDTO {
    private final String bankAccountSend;
    private final String bankAccountReceive;
    private final Double amount;
}
