package com.daniel.bluesoftbank.bank.api.moneyTransfer.service;

import com.daniel.bluesoftbank.bank.api.bankAccount.service.BankAccountService;
import com.daniel.bluesoftbank.bank.api.moneyTransfer.mapper.MoneyTransferToConsignMoney;
import com.daniel.bluesoftbank.bank.api.moneyTransfer.model.MoneyTransfer;
import com.daniel.bluesoftbank.bank.api.moneyTransfer.repository.IMoneyTransferRepository;
import com.daniel.bluesoftbank.bank.api.moneyTransfer.service.dto.external.ConsignMoneyInDTO;
import com.daniel.bluesoftbank.bank.api.moneyTransfer.service.dto.internal.ConsignMoneyOutDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class MoneyTransferService {
    @Autowired
    private final IMoneyTransferRepository repository;
    private final BankAccountService bankAccountService;
    private final MoneyTransferToConsignMoney mapperOutConsignMoney;

    public MoneyTransferService(IMoneyTransferRepository repository, BankAccountService bankAccountService, MoneyTransferToConsignMoney mapperOutConsignMoney) {
        this.repository = repository;
        this.bankAccountService = bankAccountService;
        this.mapperOutConsignMoney = mapperOutConsignMoney;
    }


    public ConsignMoneyOutDTO consignMoney(ConsignMoneyInDTO dto) {
        this.bankAccountService.addBalance(dto.getBankAccountNumber(), dto.getAmount());
        MoneyTransfer moneyTransfer = new MoneyTransfer();
        moneyTransfer.setBankAccountReceive(dto.getBankAccountNumber());
        moneyTransfer.setAmount(dto.getAmount());
        return mapperOutConsignMoney.map(this.repository.saveAndFlush(moneyTransfer));
    }

    public List<MoneyTransfer> historical(String number, int page, int size) {
        return this.repository.hitorical(number, page, size);
//        return new ArrayList<MoneyTransfer>();
    }

    public List<MoneyTransfer> monthlyStatement(String number, Date start, Date end) {
        return this.repository.monthlyStatement(number, start, end);
    }
}