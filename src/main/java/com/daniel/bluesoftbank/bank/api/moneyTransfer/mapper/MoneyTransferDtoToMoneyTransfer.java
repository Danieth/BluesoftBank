package com.daniel.bluesoftbank.bank.api.moneyTransfer.mapper;

import com.daniel.bluesoftbank.bank.api.moneyTransfer.model.MoneyTransfer;
import com.daniel.bluesoftbank.bank.api.moneyTransfer.service.dto.external.MoneyTransferInDTO;
import com.daniel.bluesoftbank.bank.utils.IMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MoneyTransferDtoToMoneyTransfer implements IMapper<MoneyTransferInDTO, MoneyTransfer> {
    @Autowired
    private final ModelMapper modelMapper;

    public MoneyTransferDtoToMoneyTransfer(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public MoneyTransfer map(MoneyTransferInDTO in) {
        return this.modelMapper.map(in, MoneyTransfer.class);
    }
}
