package com.daniel.bluesoftbank.bank.api.bankAccount.mapper;

import com.daniel.bluesoftbank.bank.api.bankAccount.model.BankAccount;
import com.daniel.bluesoftbank.bank.api.bankAccount.service.dto.internal.BankAccountOutDTO;
import com.daniel.bluesoftbank.bank.utils.IMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BankAccountOutDtoToBankAccount implements IMapper<BankAccount, BankAccountOutDTO> {

    @Autowired
    private final ModelMapper modelMapper;

    public BankAccountOutDtoToBankAccount(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public BankAccountOutDTO map(BankAccount in) {
        BankAccountOutDTO dto = this.modelMapper.map(in, BankAccountOutDTO.class);
        dto.setNumber(in.getId().toString());
        return dto;
    }
}
