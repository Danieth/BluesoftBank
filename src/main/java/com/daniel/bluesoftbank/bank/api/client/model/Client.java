package com.daniel.bluesoftbank.bank.api.client.model;

import com.daniel.bluesoftbank.bank.api.bankAccount.model.BankAccount;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.Data;

import java.util.List;

@Data
@Entity
public class Client {
    @Id
    private String dni;
    private String firstName;
    private String lastName;
    @OneToMany(mappedBy = "clientDni")
    private List<BankAccount> bankAccountList;
}
