package com.daniel.bluesoftbank.bank.api.bankAccount.mapper;

import com.daniel.bluesoftbank.bank.api.bankAccount.model.BankAccount;
import com.daniel.bluesoftbank.bank.api.bankAccount.service.dto.external.BankAccountInDTO;
import com.daniel.bluesoftbank.bank.utils.IMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BankAccountInDtoToBankAccount implements IMapper<BankAccountInDTO, BankAccount> {
    @Autowired
    private final ModelMapper modelMapper;

    public BankAccountInDtoToBankAccount(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public BankAccount map(BankAccountInDTO in) {
        return this.modelMapper.map(in, BankAccount.class);
    }
}
