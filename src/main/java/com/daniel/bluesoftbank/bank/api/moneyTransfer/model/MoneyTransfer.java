package com.daniel.bluesoftbank.bank.api.moneyTransfer.model;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDate;
import java.util.UUID;
@Data
@Entity
public class MoneyTransfer {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID uuid;
    private Double amount;
    private String bankAccountSend;
    private String bankAccountReceive;
    @Column(updatable = false)
    @CreationTimestamp()
    private LocalDate createdAt;
}
