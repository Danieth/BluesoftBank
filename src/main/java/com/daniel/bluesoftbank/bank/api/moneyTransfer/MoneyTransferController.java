package com.daniel.bluesoftbank.bank.api.moneyTransfer;

import com.daniel.bluesoftbank.bank.api.moneyTransfer.model.MoneyTransfer;
import com.daniel.bluesoftbank.bank.api.moneyTransfer.service.MoneyTransferService;
import com.daniel.bluesoftbank.bank.api.moneyTransfer.service.dto.external.ConsignMoneyInDTO;
import com.daniel.bluesoftbank.bank.api.moneyTransfer.service.dto.internal.ConsignMoneyOutDTO;
import com.daniel.bluesoftbank.bank.utils.ParseDatePath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/money-transfer")
public class MoneyTransferController {
    @Autowired
    private final MoneyTransferService service;

    public MoneyTransferController(MoneyTransferService service) {
        this.service = service;
    }

    @PostMapping("consign-money")
    public ResponseEntity<ConsignMoneyOutDTO> consignMoney(@RequestBody ConsignMoneyInDTO dto) {
        return ResponseEntity.ok(this.service.consignMoney(dto));
    }

    @GetMapping("history/{bankAccountNumber}")
    public ResponseEntity<List<MoneyTransfer>> history(@PathVariable("bankAccountNumber") String number, @RequestHeader(value = "size", defaultValue = "0") int size, @RequestHeader(value = "page", defaultValue = "10") int page) {
        return ResponseEntity.ok(this.service.historical(number, page, size));
    }

    @GetMapping("monthly-statement/{bankAccountNumber}/{startDate}/{endDate}")
    public ResponseEntity<List<MoneyTransfer>> monthlyStatement(@PathVariable("bankAccountNumber") String number, @PathVariable("startDate") String startDate, @PathVariable("endDate") String endDate) {
        ParseDatePath parseDatePath = new ParseDatePath();
        return ResponseEntity.ok(this.service.monthlyStatement(number, parseDatePath.parseDate(startDate), parseDatePath.parseDate(endDate)));
    }
}
