package com.daniel.bluesoftbank.bank.api.moneyTransfer.service.dto.internal;

import lombok.Data;

import java.time.LocalDate;
import java.util.UUID;

@Data
public class ConsignMoneyOutDTO {
    private UUID uuid;
    private Double amount;
    private String bankAccountReceive;
    private LocalDate createdAt;
}
