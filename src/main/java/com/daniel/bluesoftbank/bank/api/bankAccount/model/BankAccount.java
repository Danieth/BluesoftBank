package com.daniel.bluesoftbank.bank.api.bankAccount.model;

import com.daniel.bluesoftbank.bank.api.client.model.Client;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDate;

@Data
@Entity
public class BankAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String city;
    private Double balance;
    @ManyToOne
    @JoinColumn(name = "dni")
    private Client clientDni;
    @Column(updatable = false)
    @CreationTimestamp()
    private LocalDate createdAt;
}
